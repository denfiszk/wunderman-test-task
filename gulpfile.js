var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');
var gulpIf = require('gulp-if');
var gulp = require('gulp');

const { src, dest } = require('gulp');

exports.default = function() {
  return src('app/**/*')
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.sass', sass()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(dest('dist/'));
    
}


/*


var gulp = require('gulp');
var sass = require('gulp-sass');
//var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var useref = require('gulp-useref');

gulp.task('hello', function() {
    console.log("Hi there");
});
*/
gulp.task('sass', function(){
    return gulp.src('app/styles/style.sass')
        .pipe(sass())
        .pipe(gulp.dest('app/styles'))

});

gulp.task('watch', function(){
    gulp.watch('app/styles/style.sass', gulp.series('sass'));
});
/*

gulp.task('useref', function(){
  return gulp.src('app')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});


*/







