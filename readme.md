v1.0.0
17/02/2019
Daniel López Paterna

TEST TASK
Static website built with Bootstrap 4 and SASS Css.

REQUIREMENTS
Node.JS, NPM

DESCRIPTION
Static page built with Bootstrap and SASS, using Node.JS, NPM, Prepros and Gulp to deal with the linting, compiling and minifying.

Accross the page I have tried to use as many Bootstrap classes as possible to use as less SASS as possible. Most of the overwriting was due to the color and font size limitations that Bootstrap obviously has.

The fact the styling code was kept short made the amount of mixins and variables quite low.

Bootstrap was added as a NPM package.

The HTML code includes semantic tags where it makes sense to use them and it is 100% validated by the w3c validation service. The page is also  responsive and accessible.

The final conversion of the SASS file to CSS was made through Gulp. However, during the development of the page and for the sake of comfort, Prepros was the choice to continuosly generate the CSS file. The minifying was made with Gulp.

'app' is the folder where I worked and Gulp generated the 'dist' folder, which contains the final version. 

Site structure
```
site
│   readme.md
│   .gitignore
│   gulpfile.js
│   package.json
│   package-lock.json
│   prepros-6.config 
│
└--- app/
│   │   index.html
│   │   
│   └---images/
│   └---styles/
│   │   │
│   │   └---style.css
│   │   └---style.sass
│   │
└--- dist/
│   │   index.html
│   │   
│   └---images/
│   └---styles/
│   │   │
│   │   └---style.css
│   │
└--- (node_modules)/
    │
    └---(...)
```










